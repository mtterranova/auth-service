import db from "../database";
import bcrypt from "bcrypt";
import { IUser, IUserModel } from "../types";

const UserModel: IUserModel = {
  create: async (username: string, password: string): Promise<string> => {
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);

    return new Promise((resolve, reject) => {
      db.run("INSERT INTO users (username, password, role) VALUES (?, ?, ?)", [username, hashedPassword, 1], (err) => {
        if (err) {
          reject(err);
        } else {
          resolve(username);
        }
      });
    });
  },

  findByUsername: async (username: string): Promise<IUser | undefined> => {
    return new Promise((resolve, reject) => {
      db.get("SELECT * FROM users WHERE username = ?", [username], (err, row: IUser) => {
        if (err) {
          reject(err);
        } else {
          resolve(row);
        }
      });
    });
  },

  updateRoleByUsername: async (username: string, role: number): Promise<{ username: string; role: number }> => {
    return new Promise((resolve, reject) => {
      db.run("UPDATE users SET role = ? WHERE username = ?", [role, username], (err) => {
        if (err) {
          reject(err);
        } else {
          resolve({ username, role });
        }
      });
    });
  },
};

export default UserModel;
