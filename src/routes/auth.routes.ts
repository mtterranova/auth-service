import express from "express";
import AuthController from "../controllers/auth.controller";

const router = express.Router();

router.post("/register", (req, res) => AuthController.register(req, res));
router.post("/login", (req, res) => AuthController.login(req, res));
router.get("/verify", (req, res) => AuthController.verifyToken(req, res));
router.put("/user/edit/role", AuthController.verifyAdmin, (req, res) => AuthController.updateRole(req, res));

export default router;
