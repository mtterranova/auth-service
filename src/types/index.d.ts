import { Request, Response, NextFunction } from "express";

interface IUser {
  username: string;
  password: string;
  role: string;
}

interface IUserModel {
  create: (username: string, password: string) => Promise<string>;
  findByUsername: (username: string) => Promise<IUser | undefined>;
  updateRoleByUsername: (username: string, role: number) => Promise<{ username: string; role: number }>;
}

interface IAuthController {
  register: (req: Request, res: Response) => Promise<void>;
  login: (req: Request, res: Response) => Promise<void>;
  updateRole: (req: Request, res: Response) => Promise<void>;
  verifyToken: (req: Request, res: Response) => void;
  verifyAdmin: (req: Request, res: Response, next: NextFunction) => void;
}

export { IUser, IUserModel, IAuthController };
