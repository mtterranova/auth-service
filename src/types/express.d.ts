import "express-serve-static-core";
import { JwtPayload } from "jsonwebtoken";

interface CustomJwtPayload extends JwtPayload {
  role: number;
}

declare module "express-serve-static-core" {
  interface Request {
    decoded: CustomJwtPayload;
  }
}

export { CustomJwtPayload };
