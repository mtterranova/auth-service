import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import dotenv from "dotenv";
import { NextFunction, Request, Response } from "express";
import UserModel from "../models/user.model";
import { IAuthController, IUser } from "../types";
import { CustomJwtPayload } from "../types/express";
import { userRoles } from "../types/userRoles.enum";

dotenv.config();
const JWT_SECRET: string = process.env.JWT_SECRET!;

const AuthController: IAuthController = {
  register: async (req: Request, res: Response): Promise<void> => {
    const { username, password } = req.body;

    try {
      const userExists = await UserModel.findByUsername(username);

      if (userExists) {
        res.status(409).json({ error: "Username already exists" });
        return;
      }

      const createdUsername = await UserModel.create(username, password);
      const token = jwt.sign({ username: username, role: userRoles.User }, JWT_SECRET, {
        expiresIn: "1hr",
      });

      res.send({
        token,
        username,
        msg: `${createdUsername} registered successfully`,
      });
    } catch (error) {
      res.status(500).json({ error: "Internal Server Error: /api/register" });
      return;
    }
  },

  login: async (req: Request, res: Response): Promise<void> => {
    const { username, password } = req.body;

    try {
      const user: IUser | undefined = await UserModel.findByUsername(username);

      if (!user) {
        res.status(401).json({ error: "Invalid username or password" });
        return;
      }

      const isPasswordValid = await bcrypt.compare(password, user.password);

      if (!isPasswordValid) {
        res.status(401).json({ error: "Invalid username or password" });
        return;
      }

      const token = jwt.sign({ username: user.username, role: user.role }, JWT_SECRET, {
        expiresIn: "1hr",
      });

      res.send({ token, username, msg: `${username} is logged in` });
    } catch (error) {
      res.status(500).json({ error: "Internal Server Error: /api/login" });
      return;
    }
  },

  updateRole: async (req: Request, res: Response): Promise<void> => {
    const { username, role } = req.body;

    try {
      const updatedUser = await UserModel.updateRoleByUsername(username, role);
      res.json(updatedUser);
    } catch (error) {
      res.status(500).json({ error: "Internal Server Error: /api/updateRole" });
    }
  },

  verifyToken(req: Request, res: Response) {
    if (!req.headers.authorization) {
      return res.status(401).json({ error: "auth required" });
    }

    const token = req.headers.authorization.split(" ")[1];
    jwt.verify(token, JWT_SECRET, (err, decoded) => {
      if (err) {
        return res.status(401).json({ error: "Invalid token" });
      }
      if (isTokenExpired(decoded)) {
        res.status(401).json({ error: "Token has expired" });
        return;
      }

      res.status(200).json({ data: decoded });
    });
  },

  verifyAdmin: (req: Request, res: Response, next: NextFunction) => {
    if (!req.headers.authorization) {
      res.status(401).json({ error: "auth required" });
      return;
    }

    const token = req.headers.authorization.split(" ")[1];
    jwt.verify(token, JWT_SECRET, (err, decoded) => {
      if (err) {
        return res.status(401).json({ error: "Invalid token" });
      }
      if (isTokenExpired(decoded)) {
        res.status(401).json({ error: "Token has expired" });
        return;
      }
      const validatedToken: CustomJwtPayload = validateDecoded(decoded);
      if (validatedToken.role !== userRoles.Admin) {
        res.status(403).json({ error: "You are not authorized to perform this action" });
        return;
      }
      next();
    });
  },
};

const validateDecoded = (decoded: string | jwt.JwtPayload | undefined): CustomJwtPayload => {
  if (typeof decoded === "string" || decoded === undefined) {
    throw new Error("Decoded token is not of type jwt.JwtPayload");
  }
  return decoded as CustomJwtPayload;
};

const isTokenExpired = (decoded: string | jwt.JwtPayload | undefined): boolean => {
  const now = Math.floor(Date.now() / 1000);
  return typeof decoded === "object" && decoded?.exp !== undefined && decoded.exp < now;
};

export default AuthController;
