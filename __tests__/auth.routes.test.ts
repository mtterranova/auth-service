import request from "supertest";
import express from "express";
import router from "../src/routes/auth.routes";
import AuthController from "../src/controllers/auth.controller";

jest.mock("../src/controllers/auth.controller", () => ({
  verifyAdmin: jest.fn((req, res, next) => next()),
  admin: jest.fn((req, res) => res.send("admin area")),
  register: jest.fn((req, res) => res.send("registered")),
  login: jest.fn((req, res) => res.send("logged in")),
  verifyToken: jest.fn((req, res) => res.send("token verified")),
  updateRole: jest.fn((req, res) => res.send("role updated")),
}));

const app = express();
app.use(express.json());
app.use(router);

describe("auth.routes", () => {
  it("POST /register should use register controller", async () => {
    const response = await request(app).post("/register");
    expect(AuthController.register).toHaveBeenCalled();
    expect(response.text).toBe("registered");
  });

  it("POST /login should use login controller", async () => {
    const response = await request(app).post("/login");
    expect(AuthController.login).toHaveBeenCalled();
    expect(response.text).toBe("logged in");
  });

  it("GET /verify should use verifyToken controller", async () => {
    const response = await request(app).get("/verify");
    expect(AuthController.verifyToken).toHaveBeenCalled();
    expect(response.text).toBe("token verified");
  });

  it("PUT /user/edit/role should use verifyAdmin and updateRole controller", async () => {
    const response = await request(app).put("/user/edit/role");
    expect(AuthController.verifyAdmin).toHaveBeenCalled();
    expect(AuthController.updateRole).toHaveBeenCalled();
    expect(response.text).toBe("role updated");
  });
});
