import request from "supertest";
import express from "express";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import UserModel from "../src/models/user.model";
import AuthController from "../src/controllers/auth.controller";
import { userRoles } from "../src/types/userRoles.enum";

jest.mock("jsonwebtoken");
jest.mock("bcrypt");
jest.mock("../src/models/user.model");

const app = express();
app.use(express.json());
app.post("/register", AuthController.register);
app.post("/login", AuthController.login);
app.put("/user/edit/role", AuthController.updateRole);
app.get("/verify", AuthController.verifyToken);

describe("AuthController", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("register", () => {
    it("should register a new user and return a token", async () => {
      (UserModel.findByUsername as jest.Mock).mockResolvedValue(null);
      (UserModel.create as jest.Mock).mockResolvedValue("newUser");
      (jwt.sign as jest.Mock).mockReturnValue("fakeToken");

      const res = await request(app).post("/register").send({ username: "newUser", password: "password" });

      expect(res.status).toBe(200);
      expect(res.body).toEqual({
        token: "fakeToken",
        username: "newUser",
        msg: "newUser registered successfully",
      });
    });

    it("should return 409 if username already exists", async () => {
      (UserModel.findByUsername as jest.Mock).mockResolvedValue({ username: "existingUser" });

      const res = await request(app).post("/register").send({ username: "existingUser", password: "password" });

      expect(res.status).toBe(409);
      expect(res.body).toEqual({ error: "Username already exists" });
    });
  });

  describe("login", () => {
    it("should authenticate user and issue a token", async () => {
      const user = { username: "user", password: "hashed_password", role: userRoles.User };
      (UserModel.findByUsername as jest.Mock).mockResolvedValue(user);
      (bcrypt.compare as jest.Mock).mockResolvedValue(true);
      (jwt.sign as jest.Mock).mockReturnValue("userToken");

      const res = await request(app).post("/login").send({ username: "user", password: "password" });

      expect(res.status).toBe(200);
      expect(res.body).toEqual({
        token: "userToken",
        username: "user",
        msg: "user is logged in",
      });
    });

    it("should return 401 if password is incorrect", async () => {
      (UserModel.findByUsername as jest.Mock).mockResolvedValue({ username: "user", password: "hashed_password" });
      (bcrypt.compare as jest.Mock).mockResolvedValue(false);

      const res = await request(app).post("/login").send({ username: "user", password: "wrong" });

      expect(res.status).toBe(401);
      expect(res.body).toEqual({ error: "Invalid username or password" });
    });
  });

  describe("updateRole", () => {
    it("should update the user role", async () => {
      (UserModel.updateRoleByUsername as jest.Mock).mockResolvedValue({ username: "user", role: 2 });

      const res = await request(app).put("/user/edit/role").send({ username: "user", role: 2 });

      expect(res.status).toBe(200);
      expect(res.body).toEqual({ username: "user", role: 2 });
    });

    it("should return 500 on database error", async () => {
      (UserModel.updateRoleByUsername as jest.Mock).mockRejectedValue(new Error("DB Error"));

      const res = await request(app).put("/user/edit/role").send({ username: "user", role: 2 });

      expect(res.status).toBe(500);
      expect(res.body).toEqual({ error: "Internal Server Error: /api/updateRole" });
    });
  });

  describe("verifyToken", () => {
    it("should verify the token and return decoded data", async () => {
      (jwt.verify as jest.Mock).mockImplementation((token, secret, callback) => {
        callback(null, { username: "user" });
      });

      const res = await request(app).get("/verify").set("Authorization", "Bearer fakeToken");

      expect(res.status).toBe(200);
      expect(res.body).toEqual({ data: { username: "user" } });
    });

    it("should return 401 if token is invalid", async () => {
      (jwt.verify as jest.Mock).mockImplementation((token, secret, callback) => {
        callback(new Error("Invalid token"), null);
      });

      const res = await request(app).get("/verify").set("Authorization", "Bearer fakeToken");

      expect(res.status).toBe(401);
      expect(res.body).toEqual({ error: "Invalid token" });
    });
  });
});
