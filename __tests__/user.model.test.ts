import bcrypt from "bcrypt";
import db from "../src/database";
import UserModel from "../src/models/user.model";

jest.mock("bcrypt");
jest.mock("../src/database", () => ({
  run: jest.fn(),
  get: jest.fn(),
}));

describe("UserModel", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("create", () => {
    it("should hash password and create a user successfully", async () => {
      const username = "testuser";
      const password = "password123";
      const hashedPassword = "hashedPassword123";

      // Mock bcrypt functions
      (bcrypt.genSalt as jest.Mock).mockResolvedValue("someSalt");
      (bcrypt.hash as jest.Mock).mockResolvedValue(hashedPassword);

      // Mock database function
      (db.run as jest.Mock).mockImplementation((sql, params, callback) => {
        callback(null);
      });

      await expect(UserModel.create(username, password)).resolves.toEqual(username);
      expect(bcrypt.hash).toHaveBeenCalledWith(password, "someSalt");
      expect(db.run).toHaveBeenCalledWith(
        "INSERT INTO users (username, password, role) VALUES (?, ?, ?)",
        [username, hashedPassword, 1],
        expect.any(Function),
      );
    });

    it("should reject if the database returns an error", async () => {
      const username = "testuser";
      const password = "password123";
      const error = new Error("DB error");

      (db.run as jest.Mock).mockImplementation((sql, params, callback) => {
        callback(error);
      });

      await expect(UserModel.create(username, password)).rejects.toThrow("DB error");
    });
  });

  describe("findByUsername", () => {
    it("should return a user object when a user with the username exists", async () => {
      const fakeUser = { id: 1, username: "johnDoe", password: "hash", role: 1 };
      (db.get as jest.Mock).mockImplementation((sql, params, callback) => {
        callback(null, fakeUser);
      });

      const user = await UserModel.findByUsername("johnDoe");
      expect(user).toEqual(fakeUser);
    });

    it("should return undefined when no user with the username exists", async () => {
      (db.get as jest.Mock).mockImplementation((sql, params, callback) => {
        callback(null, undefined);
      });

      const user = await UserModel.findByUsername("janeDoe");
      expect(user).toBeUndefined();
    });

    it("should reject if there is a database error", async () => {
      const errorMessage = "Database error";
      (db.get as jest.Mock).mockImplementation((sql, params, callback) => {
        callback(new Error(errorMessage), undefined);
      });

      await expect(UserModel.findByUsername("johnDoe")).rejects.toThrow(errorMessage);
    });
  });

  describe("updateRoleByUsername", () => {
    it("should update the user's role successfully", async () => {
      (db.run as jest.Mock).mockImplementation((sql, params, callback) => {
        callback(null);
      });

      const result = await UserModel.updateRoleByUsername("johnDoe", 2);
      expect(result).toEqual({ username: "johnDoe", role: 2 });
    });

    it("should reject if there is a database error during the update", async () => {
      const errorMessage = "Database error on update";
      (db.run as jest.Mock).mockImplementation((sql, params, callback) => {
        callback(new Error(errorMessage));
      });

      await expect(UserModel.updateRoleByUsername("johnDoe", 2)).rejects.toThrow(errorMessage);
    });

    it("should handle no changes if the username does not exist", async () => {
      // This assumes the db.run callback behavior correctly handles no changes (no rows affected)
      (db.run as jest.Mock).mockImplementation((sql, params, callback) => {
        callback(null);
      });

      const result = await UserModel.updateRoleByUsername("nonexistent", 2);
      expect(result).toEqual({ username: "nonexistent", role: 2 }); // Depending on actual db.run behavior, this may need adjustment
    });
  });
});
