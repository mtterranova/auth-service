import request from "supertest";
import app from "../src/index";
import http from "http";

describe("GET /", () => {
  let server: http.Server;

  // Start the Express server before running any tests
  beforeAll(() => {
    return new Promise<void>((resolve) => {
      server = app.listen(3000, () => {
        console.log("Server is running on port 3000");
        resolve();
      });
    });
  });

  // Close the server after running all tests
  afterAll(() => {
    return new Promise<void>((resolve) => {
      server.close(() => {
        resolve();
      });
    });
  });

  it("responds with Hello, World!", async () => {
    const response = await request(app).get("/");
    expect(response.status).toBe(200);
    expect(response.text).toBe("Hello, World!");
  });
});
